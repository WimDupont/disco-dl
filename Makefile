CC = gcc

NAME = disco-dl
VERSION = 0.1

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

BIN = disco-dl
SRC = ${BIN:=.c}
OBJ = ${SRC:.c=.o}
MAN1 = ${BIN:=.1}

${OBJ}: config.h

all: ${BIN}

${BIN}: ${@:=.o}

.o: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f ${BIN} ${OBJ}

config.h:
	cp config.def.h $@

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${BIN} "${DESTDIR}${PREFIX}/bin"
	chmod 755 "${DESTDIR}${PREFIX}/bin/${BIN}"
	mkdir -p "${DESTDIR}${MANPREFIX}/man1"
	sed "s/VERSION/${VERSION}/g" < ${MAN1} > "${DESTDIR}${MANPREFIX}/man1/${MAN1}"
	chmod 644 "${DESTDIR}${MANPREFIX}/man1/${MAN1}"

uninstall:
	rm -f \
		"${DESTDIR}${PREFIX}/bin/${BIN}"\
		"${DESTDIR}${MANPREFIX}/man1/${MAN1}"

.PHONY: all clean install uninstall
